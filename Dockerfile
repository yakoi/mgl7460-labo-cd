FROM java:8

WORKDIR /

ADD build/libs/mgl7460-labo-cd.jar mgl7460-labo-cd.jar

CMD java -jar mgl7460-labo-cd.jar